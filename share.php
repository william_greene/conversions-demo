<?php

session_start();

// Redirect back to index if no campaign or no user set
if (empty($_SESSION['campaign']) ||
	empty($_SESSION['userName'])
) {
	header('Location: ./index.php');
	exit;
}

// Include view
$title = 'awe.sm Conversion Demo: Share';
require('./_includes/header.php');
require('./_includes/nav.php');
?>

<h1>Share to get people to convert</h1>

<!-- Twitter Tweet button -->


<!-- Facebook Like button -->


<!-- LinkedIn Share button -->


<?php
require('./_includes/footer.php');
?>