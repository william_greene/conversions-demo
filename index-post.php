<?php

// Store the campaign and campaign description (if provided as a new category)

session_start();
	

if (!empty($_REQUEST['newCampaignTitle'])) {
	// New Campaign
	$_SESSION['campaign'] = $_REQUEST['newCampaignTitle'];
	if (!empty($_REQUEST['newCampaignDescription']) ) {
		$_SESSION['campaign_description'] = $_REQUEST['newCampaignDescription'];
	}
} elseif(!empty($_REQUEST['campaign'])) {
	$_SESSION['campaign'] = $_REQUEST['campaign'];
	$_SESSION['campaign_description'] = '';
} else {
	// Redirect back to index
	header('Location: ./index.php');
	exit;
}

header('Location: ./signin.php');

// echo "c: {$_SESSION['campaign']} with desc: {$_SESSION['campaign_description']}";