<?php

// Include view
$title = 'awe.sm Conversion Demo: Results';
require('./_includes/header.php');
require('./_includes/nav.php');
?>

Show Results here

<p>User | Channel</p>

<table>
	<thead>
		<tr>
			<th>User</th>
			<th>Shares</th>
			<th>Page Views</th>
			<th>Conversion 1</th>
			<th>Conversion 2</th>
			<th>Conversion 3</th>
			<th>Conversion 4</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>User</td>
			<td>Shares</td>
			<td>Page Views</td>
			<td>Conversion 1</td>
			<td>Conversion 2</td>
			<td>Conversion 3</td>
			<td>Conversion 4</td>
		</tr>
	</tbody>
</table>

<?php
require('./_includes/footer.php');
?>