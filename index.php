<?php

session_start();

// Include view
$title = 'awe.sm Conversion Demo';
require('./_includes/header.php');
require('./_includes/nav.php');


// Lookup the campaigns associated with the project
// $api = new Awesm('3ee06e615fe1af9677a80015eb4d2a0343955da5031540e2061a2a44df670168');

// http://api.awe.sm/stats/range.json?v=3&key=5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9&group_by=campaign&sort_type=alpha&sort_order=asc&per_page=30&with_metadata=true&with_conversions=true
$params = array(
	'v' => 3,
	'key' => '5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9',
	'group_by' => 'campaign',
	'sort_type' => 'alpha',
	'sort_order' => 'asc',
	'per_page' => 30,
	'with_metadata' => 'true',
	'with_conversions' => 'true'
);

// Create and execute cURL request
$campaigns_request = curl_init();
curl_setopt($campaigns_request, CURLOPT_URL, 'http://api.awe.sm/stats/range.json');
curl_setopt($campaigns_request, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($campaigns_request, CURLOPT_TIMEOUT, 5);
curl_setopt($campaigns_request, CURLOPT_POST, 1);
curl_setopt($campaigns_request, CURLOPT_POSTFIELDS, $params);
$campaigns = curl_exec($campaigns_request);
$response_code = curl_getinfo($campaigns_request, CURLINFO_HTTP_CODE);
curl_close($campaigns_request);

if ($response_code == 200) {
	// Catagories retrieved. Convert JSON to PHP array.
	$campaigns = json_decode($campaigns, true);
} else {
	die("API error: HTTP {$response_code}: {$campaigns}");
}

?>

<form action="index-post.php" method="post">

	<h1>Pick a campaign</h1>
	<table>
		<thead>
			<tr>
				<th>Campaign</th>
				<th>Shares</th>
				<th>Revenue</th>
			</tr>
		</thead>
		<tbody>
<?php
	foreach($campaigns['groups'] as &$campaign) {
		if (!empty($campaign['campaign'])) {
?>
			<tr>
				<td>
					<input type="radio" name="campaign" value="<?= $campaign['campaign'] ?>" />
					<strong><?php
						if (!empty($campaign['campaign']['metadata']['name'])) {
							echo $campaign['campaign']['metadata']['name'];
						} else {
							echo $campaign['campaign'];
						}
					?></strong>
					<?php
						if (!empty($campaign['netadata']['description'])) {
							echo "<br /> {$campaign['netadata']['description']}";
						}
					?>
				</td>
				<td><?= $campaign['shares'] ?></td>
				<td>$ <?= number_format($campaign['conversions']['goal_total']['value']/100, 2, '.', ',') ?></td>
			</tr>
<?php	
		}
	}
?>
		</tbody>
	</table>

	<h2>or create a new one</h2>
	<div>
		<input name="newCampaignTitle" type="text" /><br />
		<textarea name="newCampaignDescription"></textarea>
	</div>
	
	<p><input type="submit" value="Next" /></p>

</form>

<?php
require('./_includes/footer.php');
?>