<?php

// Include view
$title = 'awe.sm Conversion Demo: Convert';
require('./_includes/header.php');
?>



<h3>Subscribe</h3>

<form action="http://groups.google.com/group/awesm-api/boxsubscribe" onsubmit="AWESM.convert('goal_1',0); return true;">
	<p>We're here for you. <a href="mailto:developers@awe.sm">Email us</a>
		<label for="email">or everyone on our developer list.</label><br />
		<input type="text" name="email" id="email" placeholder="paul@example.com" />
		<input type="submit" name="sub" value="Subscribe" />
	</p>
</form>



<div>
<script type="text/javascript" charset="utf-8"> 
twttr.events.bind('follow', function(event) { 
AWESM.convert('goal_3',0); 
}); 
</script> 

<a href="http://twitter.com/awesm" class="twitter-follow-button">Follow @awesm</a>
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
</div>
<div>
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="http://www.facebook.com/pages/awesm/61961324828" width="292" show_faces="false" border_color="" stream="false" header="false"></fb:like-box>
<script>FB.Event.subscribe('edge.create', function(response) {
AWESM.convert('goal_3',0);
});</script>
</div>
</div>



<div>
<h3>Purchase</h3>
<p><a href="#" onclick="AWESM.convert('goal_4',2500);">$25</a>&nbsp;<a href="#" onclick="AWESM.convert('goal_4',1000);">$10</a>&nbsp;<a href="#" onclick="AWESM.convert('goal_4',500);">$5</a></p>
</div>
<div>
<h3>Share</h3>
<awesm:tweet awesm:user_id="<?php echo $users[$uid]['user_id']; ?>" awesm:user_id_username="<?php echo $users[$uid]['username']; ?>" awesm:user_id_profile_url="<?php echo $users[$uid]['profile_url']; ?>" awesm:user_id_icon_url="<?php echo $users[$uid]['icon_url']; ?>" data-via="awesm" data-text="This+is+amazing!"></awesm:tweet>
<awesm:fblike awesm:user_id="<?php echo $users[$uid]['user_id']; ?>" awesm:user_id_username="<?php echo $users[$uid]['username']; ?>" awesm:user_id_profile_url="<?php echo $users[$uid]['profile_url']; ?>" awesm:user_id_icon_url="<?php echo $users[$uid]['icon_url']; ?>" show_faces="false" width="450" send="true"></awesm:fblike>
<awesm:fbshare awesm:user_id="<?php echo $users[$uid]['user_id']; ?>" awesm:user_id_username="<?php echo $users[$uid]['username']; ?>" awesm:user_id_profile_url="<?php echo $users[$uid]['profile_url']; ?>" awesm:user_id_icon_url="<?php echo $users[$uid]['icon_url']; ?>" size="small"></awesm:fbshare>

<div>
<form name="email" action="http://api.awe.sm/url/share" method="get" target="_blank">
<input type="hidden" name="v" value="3" />
<input type="hidden" name="url" value="http://www.jonathanhstrauss.com/consulting/test/snowball/conversion_demo/index.php" />
<input type="hidden" name="tool" value="mKU7uN" />
<input type="hidden" name="key" value="3ee06e615fe1af9677a80015eb4d2a0343955da5031540e2061a2a44df670168" />	<!-- demo project -->
<input type="hidden" name="channel" value="email" />
<input type="hidden" name="user_id" value="<?php echo $users[$uid]['user_id']; ?>" />
<input type="hidden" name="user_id_username" value="<?php echo $users[$uid]['username']; ?>" />
<input type="hidden" name="user_id_profile_url" value="<?php echo $users[$uid]['profile_url']; ?>" />
<input type="hidden" name="user_id_icon_url" value="<?php echo $users[$uid]['icon_url']; ?>" />
<input type="hidden" name="destination" value="mailto:?subject=This%20is%20cool!&body=I%20<3%20awe.sm!%20AWESM_URL" />
<br />
<input type="submit" value="Email" />
</form>
</div>
</div>
<script src="http://widgets.awe.sm/v2/widgets.js?key=3ee06e615fe1af9677a80015eb4d2a0343955da5031540e2061a2a44df670168"></script>



<p><a href="TODO">See results of this campaign</a></p>

<?php
require('./_includes/footer.php');
?>