<?php

session_start();

// Redirect back to index if no campaign set
if (empty($_SESSION['campaign'])) {
	header('Location: ./index.php');
	exit;
}

// Include view
$title = 'awe.sm Conversion Demo: Sign In';
require('./_includes/header.php');
require('./_includes/nav.php');

$users = array(
	array(
		'username' => 'jeremiah',
		'photo' => 'photo.jpg'
	)
);
?>

<form action="signin-post.php" method="post">

	<h1>Sign In As Someone</h1>
	<table>
		<tbody>
<?php
	foreach($users as &$user) {
?>
			<tr>
				<td>
					<input type="radio" name="userName" value="<?= $user['username'] ?>" />
				</td>
				<td>
					<img src="<?= $user['photo'] ?>" alt="<?= $user['username'] ?>" width="48" height="48" />
				</td>
				<td>
					<?= $user['username'] ?>
				</td>
			</tr>
<?php
	}
?>
		</tbody>
	</table>

<!--
	<h2>or create a new one</h2>
	<div>
		<input name="newUserName" type="text" /><br />
		newUserPhoto
	</div>
-->
	<p><input type="submit" value="Next" /></p>

</form>

<?php
require('./_includes/footer.php');
?>